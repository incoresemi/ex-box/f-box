CHANGELOG
=========

This project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[0.2.0] - 2022-08-03
--------------------
- Modified fbox interface to carry parameter indicating whether conversions should happen for fmv
- Added functions to repurpose recfn interface in designs with ieee storage format in registers.

[0.1.1] - 2022-06-22
--------------------
- Added `RegOverrides` imports to all modules.

[0.1.0] - 2022-05-09
--------------------

- Initial release for retimed FPU moudles.
