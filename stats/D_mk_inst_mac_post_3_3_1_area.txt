Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
---------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2019.2.1 (lin64) Build 2729669 Thu Dec  5 04:48:12 MST 2019
| Date         : Sat Mar  5 04:28:49 2022
| Host         : aeon running 64-bit Ubuntu 20.04 LTS
| Command      : report_utilization -hierarchical -file /scratch/git-repo/incoresemi/ex-box/f-box/build/fbox//syn_area.txt
| Design       : mk_inst_mac_post
| Device       : 7a100tcsg324-1
| Design State : Synthesized
---------------------------------------------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+----------------------+--------------------------+------------+------------+---------+------+------+--------+--------+--------------+
|       Instance       |          Module          | Total LUTs | Logic LUTs | LUTRAMs | SRLs |  FFs | RAMB36 | RAMB18 | DSP48 Blocks |
+----------------------+--------------------------+------------+------------+---------+------+------+--------+--------+--------------+
| mk_inst_mac_post     |                    (top) |       4558 |       4558 |       0 |    0 | 2023 |      0 |      0 |            0 |
|   (mk_inst_mac_post) |                    (top) |       4548 |       4548 |       0 |    0 | 2017 |      0 |      0 |            0 |
|   postmul            | mulAddRecFNToRaw_postMul |          1 |          1 |       0 |    0 |    0 |      0 |      0 |            0 |
|   rg_valid_in_0      |                   FIFOL1 |          0 |          0 |       0 |    0 |    1 |      0 |      0 |            0 |
|   rg_valid_in_1      |                 FIFOL1_0 |          2 |          2 |       0 |    0 |    1 |      0 |      0 |            0 |
|   rg_valid_in_2      |                 FIFOL1_1 |          3 |          3 |       0 |    0 |    1 |      0 |      0 |            0 |
|   rg_valid_out_0     |                 FIFOL1_2 |          1 |          1 |       0 |    0 |    1 |      0 |      0 |            0 |
|   rg_valid_out_1     |                 FIFOL1_3 |          1 |          1 |       0 |    0 |    1 |      0 |      0 |            0 |
|   rg_valid_out_2     |                 FIFOL1_4 |          2 |          2 |       0 |    0 |    1 |      0 |      0 |            0 |
+----------------------+--------------------------+------------+------------+---------+------+------+--------+--------+--------------+


