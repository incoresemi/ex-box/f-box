# Synthesis Numbers for individual stages in Hardfloat FMA

|Mod Name          |Ext|IN Buff Size|Out Buff Size|Variant(if applicable)|LUT |WNS    |
|------------------|---|------------|-------------|----------------------|----|-------|
|mk_inst_mac       |F  |1           |1            |                      |758 |-2.9   |
|mk_inst_mac       |F  |2           |1            |                      |715 |-0.474 |
|mk_inst_mac       |F  |3           |1            |                      |717 |-0.474 |
|mk_inst_mac       |F  |3           |3            |                      |721 |1.618  |
|mk_inst_mac       |F  |1           |2            |                      |713 |1.855  |
|mk_inst_mac       |F  |1           |3            |                      |715 |1.855  |
|mk_inst_mac       |F  |3           |2            |                      |712 |2.451  |
|mk_inst_mac       |F  |2           |3            |                      |719 |2.451  |
|mk_inst_mac       |F  |2           |2            |                      |708 |2.499  |
|mk_inst_mac       |D  |1           |1            |                      |3747|-8.086 |
|mk_inst_mac       |D  |2           |1            |                      |3688|-6.271 |
|mk_inst_mac       |D  |3           |1            |                      |3690|-6.271 |
|mk_inst_mac       |D  |1           |2            |                      |3807|-3.682 |
|mk_inst_mac       |D  |1           |3            |                      |3809|-3.682 |
|mk_inst_mac       |D  |2           |2            |                      |3647|-1.176 |
|mk_inst_mac       |D  |3           |2            |                      |3647|-1.176 |
|mk_inst_mac       |D  |2           |3            |                      |3648|-1.176 |
|mk_inst_mac       |D  |3           |3            |                      |3649|-1.176 |
|mk_inst_mac_post  |F  |1           |1            |2                     |1176|-12.912|
|mk_inst_mac_post  |F  |1           |1            |1                     |1161|-11.257|
|mk_inst_mac_post  |F  |1           |2            |2                     |1152|-11.198|
|mk_inst_mac_post  |F  |1           |3            |2                     |1154|-11.198|
|mk_inst_mac_post  |F  |2           |1            |2                     |1129|-10.486|
|mk_inst_mac_post  |F  |3           |1            |2                     |1131|-10.486|
|mk_inst_mac_post  |F  |1           |2            |1                     |1163|-9.917 |
|mk_inst_mac_post  |F  |1           |3            |1                     |1165|-9.917 |
|mk_inst_mac_post  |F  |1           |1            |3                     |938 |-8.917 |
|mk_inst_mac_post  |F  |1           |1            |4                     |938 |-8.917 |
|mk_inst_mac_post  |F  |2           |2            |2                     |1122|-8.772 |
|mk_inst_mac_post  |F  |3           |2            |2                     |1122|-8.772 |
|mk_inst_mac_post  |F  |2           |3            |2                     |1123|-8.772 |
|mk_inst_mac_post  |F  |3           |3            |2                     |1124|-8.772 |
|mk_inst_mac_post  |F  |2           |1            |1                     |1114|-8.711 |
|mk_inst_mac_post  |F  |3           |1            |1                     |1116|-8.711 |
|mk_inst_mac_post  |F  |2           |2            |1                     |1119|-7.399 |
|mk_inst_mac_post  |F  |3           |2            |1                     |1119|-7.399 |
|mk_inst_mac_post  |F  |2           |3            |1                     |1120|-7.399 |
|mk_inst_mac_post  |F  |3           |3            |1                     |1121|-7.399 |
|mk_inst_mac_post  |F  |2           |1            |3                     |900 |-6.491 |
|mk_inst_mac_post  |F  |2           |1            |4                     |900 |-6.491 |
|mk_inst_mac_post  |F  |3           |1            |3                     |902 |-6.491 |
|mk_inst_mac_post  |F  |3           |1            |4                     |902 |-6.491 |
|mk_inst_mac_post  |F  |1           |2            |3                     |939 |-5.08  |
|mk_inst_mac_post  |F  |1           |2            |4                     |939 |-5.08  |
|mk_inst_mac_post  |F  |1           |3            |3                     |941 |-5.08  |
|mk_inst_mac_post  |F  |1           |3            |4                     |941 |-5.08  |
|mk_inst_mac_post  |F  |2           |2            |3                     |902 |-2.654 |
|mk_inst_mac_post  |F  |3           |2            |3                     |902 |-2.654 |
|mk_inst_mac_post  |F  |2           |2            |4                     |902 |-2.654 |
|mk_inst_mac_post  |F  |3           |2            |4                     |902 |-2.654 |
|mk_inst_mac_post  |F  |2           |3            |3                     |903 |-2.654 |
|mk_inst_mac_post  |F  |2           |3            |4                     |903 |-2.654 |
|mk_inst_mac_post  |F  |3           |3            |3                     |904 |-2.654 |
|mk_inst_mac_post  |F  |3           |3            |4                     |904 |-2.654 |
|mk_inst_mac_post  |D  |1           |1            |2                     |4700|-19.746|
|mk_inst_mac_post  |D  |1           |2            |2                     |4692|-18.871|
|mk_inst_mac_post  |D  |1           |3            |2                     |4694|-18.871|
|mk_inst_mac_post  |D  |1           |1            |3                     |4465|-18.808|
|mk_inst_mac_post  |D  |1           |1            |1                     |4637|-17.349|
|mk_inst_mac_post  |D  |2           |1            |2                     |4635|-17.3  |
|mk_inst_mac_post  |D  |3           |1            |2                     |4637|-17.3  |
|mk_inst_mac_post  |D  |1           |2            |3                     |4470|-16.758|
|mk_inst_mac_post  |D  |1           |3            |3                     |4472|-16.758|
|mk_inst_mac_post  |D  |2           |2            |2                     |4677|-16.425|
|mk_inst_mac_post  |D  |3           |2            |2                     |4677|-16.425|
|mk_inst_mac_post  |D  |2           |3            |2                     |4678|-16.425|
|mk_inst_mac_post  |D  |3           |3            |2                     |4679|-16.425|
|mk_inst_mac_post  |D  |2           |1            |3                     |4401|-16.362|
|mk_inst_mac_post  |D  |3           |1            |3                     |4403|-16.362|
|mk_inst_mac_post  |D  |1           |2            |1                     |4636|-16.178|
|mk_inst_mac_post  |D  |1           |3            |1                     |4638|-16.178|
|mk_inst_mac_post  |D  |2           |1            |1                     |4567|-14.903|
|mk_inst_mac_post  |D  |3           |1            |1                     |4569|-14.903|
|mk_inst_mac_post  |D  |1           |1            |4                     |4100|-14.612|
|mk_inst_mac_post  |D  |2           |2            |3                     |4400|-14.312|
|mk_inst_mac_post  |D  |3           |2            |3                     |4400|-14.312|
|mk_inst_mac_post  |D  |2           |3            |3                     |4401|-14.312|
|mk_inst_mac_post  |D  |3           |3            |3                     |4402|-14.312|
|mk_inst_mac_post  |D  |2           |2            |1                     |4556|-13.732|
|mk_inst_mac_post  |D  |3           |2            |1                     |4556|-13.732|
|mk_inst_mac_post  |D  |2           |3            |1                     |4557|-13.732|
|mk_inst_mac_post  |D  |3           |3            |1                     |4558|-13.732|
|mk_inst_mac_post  |D  |2           |1            |4                     |4025|-12.166|
|mk_inst_mac_post  |D  |3           |1            |4                     |4027|-12.166|
|mk_inst_mac_post  |D  |1           |2            |4                     |4160|-11.351|
|mk_inst_mac_post  |D  |1           |3            |4                     |4162|-11.351|
|mk_inst_mac_post  |D  |2           |2            |4                     |4052|-8.905 |
|mk_inst_mac_post  |D  |3           |2            |4                     |4052|-8.905 |
|mk_inst_mac_post  |D  |2           |3            |4                     |4053|-8.905 |
|mk_inst_mac_post  |D  |3           |3            |4                     |4054|-8.905 |
|mk_inst_post      |F  |1           |1            |2                     |389 |-1.264 |
|mk_inst_post      |F  |1           |1            |1                     |395 |0.324  |
|mk_inst_post      |F  |1           |2            |2                     |386 |0.748  |
|mk_inst_post      |F  |1           |3            |2                     |388 |0.748  |
|mk_inst_post      |F  |3           |3            |1                     |383 |1.623  |
|mk_inst_post      |F  |3           |3            |2                     |384 |1.626  |
|mk_inst_post      |F  |3           |3            |3                     |177 |1.638  |
|mk_inst_post      |F  |3           |3            |4                     |177 |1.638  |
|mk_inst_post      |F  |3           |2            |3                     |174 |2.451  |
|mk_inst_post      |F  |3           |2            |4                     |174 |2.451  |
|mk_inst_post      |F  |2           |3            |3                     |175 |2.451  |
|mk_inst_post      |F  |2           |3            |4                     |175 |2.451  |
|mk_inst_post      |F  |2           |3            |2                     |382 |2.451  |
|mk_inst_post      |F  |3           |2            |1                     |390 |2.451  |
|mk_inst_post      |F  |2           |3            |1                     |392 |2.451  |
|mk_inst_post      |F  |3           |2            |2                     |397 |2.451  |
|mk_inst_post      |F  |1           |2            |3                     |169 |2.478  |
|mk_inst_post      |F  |1           |2            |4                     |169 |2.478  |
|mk_inst_post      |F  |1           |2            |1                     |386 |2.478  |
|mk_inst_post      |F  |2           |2            |3                     |170 |2.499  |
|mk_inst_post      |F  |2           |2            |4                     |170 |2.499  |
|mk_inst_post      |F  |1           |3            |3                     |173 |2.499  |
|mk_inst_post      |F  |1           |3            |4                     |173 |2.499  |
|mk_inst_post      |F  |3           |1            |3                     |174 |2.499  |
|mk_inst_post      |F  |3           |1            |4                     |174 |2.499  |
|mk_inst_post      |F  |2           |2            |2                     |379 |2.499  |
|mk_inst_post      |F  |1           |3            |1                     |388 |2.499  |
|mk_inst_post      |F  |2           |2            |1                     |388 |2.499  |
|mk_inst_post      |F  |3           |1            |2                     |392 |2.499  |
|mk_inst_post      |F  |3           |1            |1                     |394 |2.499  |
|mk_inst_post      |F  |2           |1            |3                     |171 |2.502  |
|mk_inst_post      |F  |2           |1            |4                     |171 |2.502  |
|mk_inst_post      |F  |2           |1            |2                     |391 |2.502  |
|mk_inst_post      |F  |2           |1            |1                     |398 |2.502  |
|mk_inst_post      |F  |1           |1            |3                     |170 |2.54   |
|mk_inst_post      |F  |1           |1            |4                     |170 |2.54   |
|mk_inst_post      |D  |1           |1            |2                     |1024|-4.407 |
|mk_inst_post      |D  |1           |2            |2                     |986 |-3.598 |
|mk_inst_post      |D  |1           |3            |2                     |987 |-3.598 |
|mk_inst_post      |D  |1           |1            |3                     |763 |-2.735 |
|mk_inst_post      |D  |1           |1            |1                     |931 |-2.272 |
|mk_inst_post      |D  |1           |2            |1                     |908 |-1.064 |
|mk_inst_post      |D  |1           |3            |1                     |909 |-1.064 |
|mk_inst_post      |D  |1           |2            |3                     |725 |-0.835 |
|mk_inst_post      |D  |1           |3            |3                     |726 |-0.835 |
|mk_inst_post      |D  |2           |1            |2                     |956 |0.021  |
|mk_inst_post      |D  |3           |1            |2                     |957 |0.021  |
|mk_inst_post      |D  |2           |1            |3                     |729 |0.925  |
|mk_inst_post      |D  |2           |2            |2                     |962 |0.925  |
|mk_inst_post      |D  |3           |2            |2                     |962 |0.925  |
|mk_inst_post      |D  |2           |3            |2                     |963 |0.925  |
|mk_inst_post      |D  |3           |3            |2                     |964 |0.925  |
|mk_inst_post      |D  |1           |1            |4                     |419 |0.992  |
|mk_inst_post      |D  |2           |3            |3                     |735 |1.616  |
|mk_inst_post      |D  |3           |3            |3                     |735 |1.616  |
|mk_inst_post      |D  |2           |3            |1                     |916 |1.619  |
|mk_inst_post      |D  |3           |3            |1                     |917 |1.619  |
|mk_inst_post      |D  |2           |3            |4                     |414 |1.63   |
|mk_inst_post      |D  |3           |3            |4                     |415 |1.63   |
|mk_inst_post      |D  |3           |2            |3                     |733 |1.775  |
|mk_inst_post      |D  |3           |2            |1                     |915 |1.778  |
|mk_inst_post      |D  |3           |2            |4                     |413 |1.789  |
|mk_inst_post      |D  |3           |1            |3                     |730 |1.804  |
|mk_inst_post      |D  |3           |1            |1                     |934 |1.996  |
|mk_inst_post      |D  |2           |1            |1                     |934 |2.06   |
|mk_inst_post      |D  |3           |1            |4                     |421 |2.456  |
|mk_inst_post      |D  |2           |2            |4                     |413 |2.462  |
|mk_inst_post      |D  |2           |2            |3                     |734 |2.462  |
|mk_inst_post      |D  |2           |2            |1                     |915 |2.462  |
|mk_inst_post      |D  |1           |2            |4                     |411 |2.478  |
|mk_inst_post      |D  |2           |1            |4                     |420 |2.478  |
|mk_inst_post      |D  |1           |3            |4                     |412 |2.499  |
|mk_inst_post_round|F  |1           |1            |2                     |679 |-9.865 |
|mk_inst_post_round|F  |1           |1            |3                     |401 |-8.872 |
|mk_inst_post_round|F  |1           |1            |4                     |401 |-8.872 |
|mk_inst_post_round|F  |1           |1            |1                     |687 |-8.591 |
|mk_inst_post_round|F  |1           |2            |3                     |396 |-7.691 |
|mk_inst_post_round|F  |1           |2            |4                     |396 |-7.691 |
|mk_inst_post_round|F  |1           |3            |3                     |398 |-7.691 |
|mk_inst_post_round|F  |1           |3            |4                     |398 |-7.691 |
|mk_inst_post_round|F  |1           |2            |2                     |669 |-7.145 |
|mk_inst_post_round|F  |1           |3            |2                     |671 |-7.145 |
|mk_inst_post_round|F  |1           |2            |1                     |678 |-5.867 |
|mk_inst_post_round|F  |1           |3            |1                     |680 |-5.867 |
|mk_inst_post_round|F  |2           |1            |2                     |662 |-1.639 |
|mk_inst_post_round|F  |2           |1            |3                     |393 |-1.448 |
|mk_inst_post_round|F  |2           |1            |4                     |393 |-1.448 |
|mk_inst_post_round|F  |2           |1            |1                     |675 |-0.939 |
|mk_inst_post_round|F  |2           |2            |2                     |668 |-0.705 |
|mk_inst_post_round|F  |2           |3            |2                     |671 |-0.705 |
|mk_inst_post_round|F  |2           |2            |4                     |393 |-0.235 |
|mk_inst_post_round|F  |2           |2            |3                     |407 |-0.218 |
|mk_inst_post_round|F  |2           |3            |3                     |410 |-0.218 |
|mk_inst_post_round|F  |2           |3            |4                     |410 |-0.218 |
|mk_inst_post_round|F  |3           |1            |2                     |672 |0.46   |
|mk_inst_post_round|F  |2           |2            |1                     |669 |0.583  |
|mk_inst_post_round|F  |2           |3            |1                     |672 |0.583  |
|mk_inst_post_round|F  |3           |2            |2                     |672 |0.998  |
|mk_inst_post_round|F  |3           |3            |2                     |673 |0.998  |
|mk_inst_post_round|F  |3           |2            |1                     |660 |1.565  |
|mk_inst_post_round|F  |3           |3            |1                     |662 |1.565  |
|mk_inst_post_round|F  |3           |3            |3                     |410 |1.646  |
|mk_inst_post_round|F  |3           |3            |4                     |410 |1.646  |
|mk_inst_post_round|F  |3           |2            |3                     |409 |1.846  |
|mk_inst_post_round|F  |3           |2            |4                     |409 |1.846  |
|mk_inst_post_round|F  |3           |1            |3                     |410 |2.128  |
|mk_inst_post_round|F  |3           |1            |4                     |410 |2.128  |
|mk_inst_post_round|F  |3           |1            |1                     |668 |2.499  |
|mk_inst_post_round|D  |1           |1            |2                     |1549|-15.201|
|mk_inst_post_round|D  |1           |2            |2                     |1548|-14.569|
|mk_inst_post_round|D  |1           |3            |2                     |1549|-14.569|
|mk_inst_post_round|D  |1           |1            |3                     |1400|-14.482|
|mk_inst_post_round|D  |1           |1            |1                     |1457|-12.559|
|mk_inst_post_round|D  |1           |1            |4                     |921 |-12.339|
|mk_inst_post_round|D  |1           |2            |1                     |1460|-11.927|
|mk_inst_post_round|D  |1           |3            |1                     |1461|-11.927|
|mk_inst_post_round|D  |1           |2            |4                     |919 |-11.701|
|mk_inst_post_round|D  |1           |3            |4                     |920 |-11.701|
|mk_inst_post_round|D  |1           |2            |3                     |1405|-10.708|
|mk_inst_post_round|D  |1           |3            |3                     |1406|-10.708|
|mk_inst_post_round|D  |2           |1            |2                     |1549|-5.075 |
|mk_inst_post_round|D  |2           |2            |2                     |1540|-4.443 |
|mk_inst_post_round|D  |2           |3            |2                     |1541|-4.443 |
|mk_inst_post_round|D  |2           |1            |3                     |1397|-3.946 |
|mk_inst_post_round|D  |2           |1            |1                     |1489|-3.24  |
|mk_inst_post_round|D  |2           |1            |4                     |927 |-2.991 |
|mk_inst_post_round|D  |2           |2            |1                     |1474|-2.844 |
|mk_inst_post_round|D  |3           |2            |1                     |1474|-2.844 |
|mk_inst_post_round|D  |2           |3            |1                     |1475|-2.844 |
|mk_inst_post_round|D  |3           |3            |1                     |1476|-2.844 |
|mk_inst_post_round|D  |3           |1            |3                     |1370|-2.823 |
|mk_inst_post_round|D  |2           |2            |4                     |919 |-2.509 |
|mk_inst_post_round|D  |2           |3            |4                     |920 |-2.509 |
|mk_inst_post_round|D  |2           |2            |3                     |1378|-2.086 |
|mk_inst_post_round|D  |2           |3            |3                     |1379|-2.086 |
|mk_inst_post_round|D  |3           |1            |1                     |1419|-1.659 |
|mk_inst_post_round|D  |3           |2            |2                     |1512|-1.431 |
|mk_inst_post_round|D  |3           |3            |2                     |1514|-1.431 |
|mk_inst_post_round|D  |3           |1            |2                     |1527|-1.267 |
|mk_inst_post_round|D  |3           |2            |3                     |1387|-0.617 |
|mk_inst_post_round|D  |3           |3            |3                     |1389|-0.617 |
|mk_inst_post_round|D  |3           |1            |4                     |913 |0.418  |
|mk_inst_post_round|D  |3           |2            |4                     |913 |0.508  |
|mk_inst_post_round|D  |3           |3            |4                     |915 |0.508  |
|mk_inst_pre       |F  |1           |1            |                      |318 |1.144  |
|mk_inst_pre       |F  |3           |2            |                      |322 |1.813  |
|mk_inst_pre       |F  |2           |3            |                      |320 |1.821  |
|mk_inst_pre       |F  |3           |3            |                      |321 |1.924  |
|mk_inst_pre       |F  |1           |3            |                      |319 |2.453  |
|mk_inst_pre       |F  |2           |2            |                      |321 |2.453  |
|mk_inst_pre       |F  |3           |1            |                      |323 |2.456  |
|mk_inst_pre       |F  |2           |1            |                      |320 |2.478  |
|mk_inst_pre       |F  |1           |2            |                      |317 |2.577  |
|mk_inst_pre       |D  |1           |1            |                      |684 |0.753  |
|mk_inst_pre       |D  |3           |2            |                      |687 |1.803  |
|mk_inst_pre       |D  |2           |3            |                      |688 |1.803  |
|mk_inst_pre       |D  |3           |3            |                      |686 |1.914  |
|mk_inst_pre       |D  |1           |3            |                      |681 |2.453  |
|mk_inst_pre       |D  |2           |2            |                      |687 |2.453  |
|mk_inst_pre       |D  |3           |1            |                      |689 |2.456  |
|mk_inst_pre       |D  |2           |1            |                      |685 |2.478  |
|mk_inst_pre       |D  |1           |2            |                      |682 |2.577  |
|mk_inst_pre_mac   |F  |1           |1            |                      |1072|-2.9   |
|mk_inst_pre_mac   |F  |2           |1            |                      |1028|-0.474 |
|mk_inst_pre_mac   |F  |3           |1            |                      |1030|-0.474 |
|mk_inst_pre_mac   |F  |1           |2            |                      |1026|1.582  |
|mk_inst_pre_mac   |F  |1           |3            |                      |1028|1.582  |
|mk_inst_pre_mac   |F  |3           |3            |                      |1034|1.617  |
|mk_inst_pre_mac   |F  |3           |2            |                      |1024|2.451  |
|mk_inst_pre_mac   |F  |2           |3            |                      |1032|2.451  |
|mk_inst_pre_mac   |F  |2           |2            |                      |1020|2.499  |
|mk_inst_pre_mac   |D  |1           |1            |                      |4438|-8.086 |
|mk_inst_pre_mac   |D  |2           |1            |                      |4381|-5.64  |
|mk_inst_pre_mac   |D  |3           |1            |                      |4383|-5.64  |
|mk_inst_pre_mac   |D  |1           |2            |                      |4464|-4.037 |
|mk_inst_pre_mac   |D  |1           |3            |                      |4466|-4.037 |
|mk_inst_pre_mac   |D  |2           |2            |                      |4318|-1.478 |
|mk_inst_pre_mac   |D  |2           |3            |                      |4319|-1.478 |
|mk_inst_pre_mac   |D  |3           |2            |                      |4319|-1.478 |
|mk_inst_pre_mac   |D  |3           |3            |                      |4320|-1.478 |
|mk_inst_round     |F  |1           |1            |                      |231 |-1.651 |
|mk_inst_round     |F  |1           |2            |                      |233 |1.281  |
|mk_inst_round     |F  |1           |3            |                      |235 |1.281  |
|mk_inst_round     |F  |3           |3            |                      |238 |1.64   |
|mk_inst_round     |F  |3           |2            |                      |224 |2.451  |
|mk_inst_round     |F  |2           |3            |                      |237 |2.451  |
|mk_inst_round     |F  |2           |2            |                      |221 |2.499  |
|mk_inst_round     |F  |3           |1            |                      |222 |2.499  |
|mk_inst_round     |F  |2           |1            |                      |232 |2.502  |
|mk_inst_round     |D  |1           |1            |                      |588 |-3.653 |
|mk_inst_round     |D  |1           |2            |                      |587 |-2.423 |
|mk_inst_round     |D  |1           |3            |                      |588 |-2.423 |
|mk_inst_round     |D  |3           |3            |                      |571 |1.617  |
|mk_inst_round     |D  |3           |2            |                      |571 |2.451  |
|mk_inst_round     |D  |2           |3            |                      |581 |2.451  |
|mk_inst_round     |D  |3           |1            |                      |568 |2.475  |
|mk_inst_round     |D  |2           |2            |                      |579 |2.499  |
|mk_inst_round     |D  |2           |1            |                      |578 |2.502  |

