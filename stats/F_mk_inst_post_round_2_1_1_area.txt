Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
---------------------------------------------------------------------------------------------------------------------------
| Tool Version : Vivado v.2019.2.1 (lin64) Build 2729669 Thu Dec  5 04:48:12 MST 2019
| Date         : Fri Mar  4 17:45:34 2022
| Host         : aeon running 64-bit Ubuntu 20.04 LTS
| Command      : report_utilization -hierarchical -file /scratch/git-repo/incoresemi/ex-box/f-box/build/fbox//syn_area.txt
| Design       : mk_inst_post_round
| Device       : 7a100tcsg324-1
| Design State : Synthesized
---------------------------------------------------------------------------------------------------------------------------

Utilization Design Information

Table of Contents
-----------------
1. Utilization by Hierarchy

1. Utilization by Hierarchy
---------------------------

+------------------------------+-----------------------------+------------+------------+---------+------+-----+--------+--------+--------------+
|           Instance           |            Module           | Total LUTs | Logic LUTs | LUTRAMs | SRLs | FFs | RAMB36 | RAMB18 | DSP48 Blocks |
+------------------------------+-----------------------------+------------+------------+---------+------+-----+--------+--------+--------------+
| mk_inst_post_round           |                       (top) |        675 |        675 |       0 |    0 | 329 |      0 |      0 |            0 |
|   (mk_inst_post_round)       |                       (top) |        297 |        297 |       0 |    0 | 326 |      0 |      0 |            0 |
|   postmul                    |    mulAddRecFNToRaw_postMul |         69 |         69 |       0 |    0 |   0 |      0 |      0 |            0 |
|   rg_valid_in_0              |                      FIFOL1 |          2 |          2 |       0 |    0 |   1 |      0 |      0 |            0 |
|   rg_valid_in_1              |                    FIFOL1_0 |          2 |          2 |       0 |    0 |   1 |      0 |      0 |            0 |
|   rg_valid_out_0             |                    FIFOL1_1 |          1 |          1 |       0 |    0 |   1 |      0 |      0 |            0 |
|   round                      |           roundRawFNToRecFN |        304 |        304 |       0 |    0 |   0 |      0 |      0 |            0 |
|     roundAnyRawFNToRecFN     |        roundAnyRawFNToRecFN |        304 |        304 |       0 |    0 |   0 |      0 |      0 |            0 |
|       (roundAnyRawFNToRecFN) |        roundAnyRawFNToRecFN |        158 |        158 |       0 |    0 |   0 |      0 |      0 |            0 |
|       lowMask_roundMask      | lowMaskLoHi__parameterized1 |        146 |        146 |       0 |    0 |   0 |      0 |      0 |            0 |
+------------------------------+-----------------------------+------------+------------+---------+------+-----+--------+--------+--------------+


